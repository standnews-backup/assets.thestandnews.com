const express = require('express')
const axios = require('axios')
const cors = require('cors')
const fs = require('fs')
const system = require('system-commands')
const app = express()
app.use(cors())
app.use(express.static('.'))
app.use(express.static('.',{extensions:['html']}))
app.use(function(req, res, next) {
    axios.get('https://archive.org/wayback/available?url=assets.thestandnews.com'+req.path)
    .then(async (res1) => {
       if (!Object.keys(res1.data.archived_snapshots).length) {
         res.status(404)
         res.send("image not available here and on archive.org")
       }
       else {
         cmd = "mkdir -p "+req.path.replace('/','')+" && curl "+res1.data.archived_snapshots.closest.url.replace('/https://assets','if_/https://assets')+" -o "+req.path.replace('/','')
         console.log(cmd)
         await system(cmd)
         res.redirect(req.path)
       }
   })
})
app.listen(3001)
