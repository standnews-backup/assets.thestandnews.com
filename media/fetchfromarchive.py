import requests
import json
import os
from os.path import exists
with open('list.txt') as file:
  for line in file:
    try:
      path = line.replace('https://assets.thestandnews.com/','').replace('\n','')
      if line.startswith('http') and not exists(path):
        x = requests.get('https://archive.org/wayback/available?url='+line.replace('\n', '').replace('https://',''))
        y = json.loads(x.text)
        if (y["archived_snapshots"] != {}):
          os.system('mkdir -p '+path.replace(line.split('/')[-1].replace('\n',''),''))
          os.system('curl '+y["archived_snapshots"]["closest"]["url"].replace('/https://assets.thestandnews.com', '_id/https://assets.thestandnews.com')+ ' -o '+path)
    except:
      pass
