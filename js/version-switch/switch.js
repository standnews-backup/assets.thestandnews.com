var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

"use strict";(function(){var SCRIPT_ENABLED=window.STANDNEWS_VERSION_SWITCH_ENABLED;var COOKIE_DOMAIN=window.STANDNEWS_DOMAIN||"thestandnews.com";var SITE_VERSION_COOKIE_KEY="_standnews_site_version_210525";var SNOOZE_BETA_NOTICE_COOKIE_KEY="_standnews_snooze_beta_notice";var SWITCH_RATIO=1;var DISABLE_REDIRECT=false;function _shouldRedirect(options){var _ref=options!==null&&options!==void 0?options:{},_ref$articlePublishTi=_ref.articlePublishTimeThreshold,articlePublishTimeThreshold=_ref$articlePublishTi===void 0?1000*60*10:_ref$articlePublishTi;if(!window.STANDNEWS_V1_ARTICLE_INFO){return true}var publishDateString=window.STANDNEWS_V1_ARTICLE_INFO.publish_date;try{var publishDate=new Date(publishDateString);return Date.now()-publishDate>articlePublishTimeThreshold}catch(_unused){return false}}function _getCookie(name){var matches=document.cookie.match(new RegExp("(?:^|; )"+name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,"\\$1")+"=([^;]*)"));return matches?decodeURIComponent(matches[1]):undefined}function _setCookie(name,value){var options=arguments.length>2&&arguments[2]!==undefined?arguments[2]:{};if(options.expires instanceof Date){options.expires=options.expires.toUTCString()}var updatedCookie=encodeURIComponent(name)+"="+encodeURIComponent(value);for(var optionKey in options){updatedCookie+="; "+optionKey;var optionValue=options[optionKey];if(optionValue!==true){updatedCookie+="="+optionValue}}document.cookie=updatedCookie}function getSiteVersionCookie(){return _getCookie(SITE_VERSION_COOKIE_KEY)}function setSiteVersionCookie(version){var maxAge=version==="2"?60*60*24*360:60*60*24*30;_setCookie(SITE_VERSION_COOKIE_KEY,version,{samesite:"strict",domain:COOKIE_DOMAIN,"max-age":maxAge})}function getSnoozeBetaNoticeCookie(){return _getCookie(SNOOZE_BETA_NOTICE_COOKIE_KEY)}function setSnoozeBetaNoticeCookie(){_setCookie(SNOOZE_BETA_NOTICE_COOKIE_KEY,true,{samesite:"strict",domain:COOKIE_DOMAIN,"max-age":60*60*24})}function redirectV1(){if(DISABLE_REDIRECT){return}var baseUrl=window.STANDNEWS_V1_BASE_URL||"http://web.archive.org/web/20210706150320/https://www.thestandnews.com/";var unsupportedPathPatterns=[/^\/404/,/^\/.*\/news\/page\//,/^\/.*\/blog\/page\//,/^\/.*\/page\//,/^\/blogs/,/^\/date/,/^\/live/,/^\/media\/photo/,/^\/media\/video/,/^\/static/];if(unsupportedPathPatterns.some(function(pattern){return pattern.test(location.pathname)})){window.location.assign(baseUrl)}else{var url=new URL(location.pathname,baseUrl);window.location.assign(url)}}function redirectV2(){if(DISABLE_REDIRECT){return}var shouldRedirect=_shouldRedirect();if(!shouldRedirect){return}var baseUrl=window.STANDNEWS_V2_BASE_URL||"http://web.archive.org/web/20210706150320/https://beta.thestandnews.com/";var url=new URL(location.pathname,baseUrl);window.location.assign(url)}if(typeof window!=="undefined"){window.standnewsVS={getSiteVersionCookie:getSiteVersionCookie,setSiteVersionCookie:setSiteVersionCookie,getSnoozeBetaNoticeCookie:getSnoozeBetaNoticeCookie,setSnoozeBetaNoticeCookie:setSnoozeBetaNoticeCookie,redirectV1:redirectV1,redirectV2:redirectV2,enabled:SCRIPT_ENABLED};if(!SCRIPT_ENABLED){return}var siteVersion=window.STANDNEWS_SITE_VERSION||"1";if(siteVersion!=="1"){return}var siteVersionCookie=standnewsVS.getSiteVersionCookie();if(!siteVersionCookie){siteVersionCookie=Math.random()<SWITCH_RATIO?"2":"1";setSiteVersionCookie(siteVersionCookie)}if(siteVersionCookie==="2"){standnewsVS.redirectV2()}}})();


}
/*
     FILE ARCHIVED ON 15:03:20 Jul 06, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 09:58:58 Dec 30, 2021.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 91.857
  exclusion.robots: 0.096
  exclusion.robots.policy: 0.089
  RedisCDXSource: 1.919
  esindex: 0.008
  LoadShardBlock: 57.434 (3)
  PetaboxLoader3.datanode: 55.421 (4)
  CDXLines.iter: 27.014 (3)
  load_resource: 75.747
  PetaboxLoader3.resolve: 29.771
*/