# Deprecated. There are too many image files. Please use [standnews-backup/standnews-assets](https://gitlab.com/standnews-backup/standnews-assets) to download images on-demand.
You can still use this repository to find missing files, but the chances are not high.

# About
Most files are downloaded from The Stand News (assets.thestandnews.com), and are copyrighted the original author (see LICENSE).
This repository is intended to back up images from assets.thestandnews.com so the back up version of www.thestandnews.com would work properly.
The maintainer(me) takes no responsibility and legal liability for any use of any files in this repository.
# Deployment
Just use a static server like gitlab pages.
# Comtributions
If you find files missing in this repo (or the web repo [standnews-backup/www.thestandnews.com](https://gitlab.com/standnews-backup/www.thestandnews.com) and you have those, please submit a merge request, or send a email to standnewsbackup@protonmail.com (not checked often). Thank you.
